

def find_sex(who):
    list_of_female_animals = ['sheep', 'fox', 'horse', 'bunny']
    list_of_male_animals = ['beaver', 'eagle', 'kitten', 'porcupine']

    if who in list_of_female_animals:
        return 'female'
    else:
        return 'male'


def greet_girl(who):

    girl = 'Hello Mis '+ who + '!'

    return girl



def greet_boy(who):

    boy = 'Hello Mir ' + who + '!'

    return boy



def greet(who):

    sex = find_sex(who)
    if sex == 'female':
        return greet_girl(who)
    else:
        return greet_boy(who)

