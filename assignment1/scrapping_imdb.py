import requests
from bs4 import BeautifulSoup

def download_webpage_content(url):
    page = requests.get(url)

    return page
# print(download_webpage_content('https://www.imdb.com/title/tt1898069/'))


def find_film_description(url):

    content = download_webpage_content(url)

    soup = BeautifulSoup(content.text, 'html.parser')

    soup2 = soup.find_all(class_= 'summary_text')
    return soup2
# print(find_film_description('https://www.imdb.com/title/tt7798634/?ref_=inth_ov_tt'))
