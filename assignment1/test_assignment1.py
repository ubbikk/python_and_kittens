from scrapping_imdb import find_film_description

def test1():
    url = 'https://www.imdb.com/title/tt6189022/?ref_=inth_ov_tt'
    description = find_film_description(url)

    print(description)


def test2():
    url = 'https://www.imdb.com/title/tt8186318/?ref_=inth_ov_tt'
    description = find_film_description(url)

    print(description)

test1()
test2()
