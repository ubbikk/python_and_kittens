from downloading_image import save_img_to_file


def test1():
    url = 'https://img15.lostpic.net/2019/08/16/3433219d0da5b062bfc943b110688955.jpg'
    file_name = 'new.jpeg'
    save_img_to_file(url, file_name)

def test2():
    url = 'https://m.media-amazon.com/images/M/MV5BMzU2ZDZlMjctNmIxYi00NDlmLWFmNjItNTBhNDA0MmRlNDgwXkEyXkFqcGdeQXRyYW5zY29kZS13b3JrZmxvdw@@._V1_UX477_CR0,0,477,268_AL_.jpg'
    file_name = 'new2.jpeg'
    save_img_to_file(url, file_name)

test1()
test2()
